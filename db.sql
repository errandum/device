CREATE TABLE IF NOT EXISTS device
(
    device_brand text COLLATE pg_catalog."default" NOT NULL,
    device_id uuid NOT NULL,
    device_name text COLLATE pg_catalog."default" NOT NULL,
    device_creation_time timestamp without time zone NOT NULL,
    CONSTRAINT device_pkey PRIMARY KEY (device_id)
)

TABLESPACE pg_default;

create index if not exists device_brand_name on device (device_brand);

ALTER TABLE device
    OWNER to postgres;
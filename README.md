To run this application, simply follow these steps from the root of the project:

`./gradlew clean build`

This will build and also run all tests (both functional and unit).

You can run the application by installing postgres manually and creating the database / table manually, but the preferred way is:

`docker-compose up`

this will build an image with the generated jar and configure a database

Some things are missing:
- Javadoc
- Comments
- Some DRY (especially in the tests)
- Test data

A postman collection is included that has all calls I used for tests. Nothing fancy, no environment or variables, but it is a good start for manual tests
package truphone.device.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import truphone.device.exception.BadRequestException;
import truphone.device.exception.DeviceNotFoundException;
import truphone.device.model.Device;
import truphone.device.model.DeviceDTO;
import truphone.device.repository.DeviceRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SearchService {

    private final DeviceRepository deviceRepository;

    @Autowired
    public SearchService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    public List<DeviceDTO> getDeviceByBrand(String deviceBrand) {

        return deviceRepository
                .findByDeviceBrandStartsWith(deviceBrand)
                .parallelStream()
                .map(DeviceDTO::getDeviceDtoFromDevice)
                .collect(Collectors.toList());
    }
}

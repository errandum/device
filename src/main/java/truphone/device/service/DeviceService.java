package truphone.device.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import truphone.device.exception.BadRequestException;
import truphone.device.exception.DeviceNotFoundException;
import truphone.device.model.Device;
import truphone.device.model.DeviceDTO;
import truphone.device.repository.DeviceRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class DeviceService {

    private final DeviceRepository deviceRepository;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    public DeviceDTO getDevice(String deviceId) throws DeviceNotFoundException {
        try {
            UUID id = UUID.fromString(deviceId);

            return DeviceDTO.getDeviceDtoFromDevice(deviceRepository.findById(id)
                    .orElseThrow(DeviceNotFoundException::new));

        } catch (IllegalArgumentException exception) {
            throw new DeviceNotFoundException();
        }
    }

    public DeviceDTO saveDevice(DeviceDTO deviceDto) throws BadRequestException {
        try {
            return DeviceDTO.getDeviceDtoFromDevice(deviceRepository.save(Device.getDeviceFromDto(deviceDto)));
        } catch (DataIntegrityViolationException e) {
            throw new BadRequestException();
        }
    }

    public DeviceDTO updateDevice(String deviceId, DeviceDTO deviceDto) throws BadRequestException, DeviceNotFoundException {
        try {
            UUID id = UUID.fromString(deviceId);
            Device toUpdate = deviceRepository.findById(id).map(
                    device -> device.mergeWithDto(deviceDto)
            ).orElseThrow(DeviceNotFoundException::new);
            return DeviceDTO.getDeviceDtoFromDevice(deviceRepository.save(toUpdate));
        } catch (DataIntegrityViolationException e) {
            throw new BadRequestException();
        } catch (IllegalArgumentException exception) {
            throw new DeviceNotFoundException();
        }
    }

    public void deleteDevice(String deviceId) throws DeviceNotFoundException {
        try {
            UUID id = UUID.fromString(deviceId);

            deviceRepository.deleteById(id);

        } catch (IllegalArgumentException exception) {
            throw new DeviceNotFoundException();
        }
    }

    public List<DeviceDTO> getAll() {
        List<DeviceDTO> devices = new ArrayList<>();
        // it's an iterator not a list, so no stream/map/collect
        deviceRepository.findAll().forEach(device -> devices.add(DeviceDTO.getDeviceDtoFromDevice(device)));
        return devices;
    }
}

package truphone.device.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDTO {

    private UUID deviceId;

    private String deviceName;

    private String deviceBrand;

    private LocalDateTime deviceCreationTime;

    public static DeviceDTO getDeviceDtoFromDevice(Device device) {
        return new DeviceDTO(
                device.getDeviceId(),
                device.getDeviceName(),
                device.getDeviceBrand(),
                device.getDeviceCreationTime()
        );
    }
}

package truphone.device.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;
import truphone.device.exception.DeviceNotFoundException;
import truphone.device.model.DeviceDTO;
import truphone.device.service.DeviceService;
import truphone.device.service.SearchService;

import java.util.List;

@RestController
@RequestMapping("search")
@Slf4j
public class SearchController {

    private final SearchService searchService;

    @Autowired
    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    // The alternative  was to have a @RequestParam and do ?brand=<brannd>, but I like this best
    @GetMapping("/brand/{deviceBrand}")
    public @ResponseBody
    List<DeviceDTO> getDeviceByBrand(@PathVariable String deviceBrand) throws DeviceNotFoundException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            log.info("Fetching devices with brand {}", deviceBrand);
            return searchService.getDeviceByBrand(deviceBrand);
        } finally {
            stopWatch.stop();
            log.info("Took {}ms to fetch items with brand {}", stopWatch.getTotalTimeMillis(), deviceBrand);
        }
    }
}

package truphone.device.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;
import truphone.device.exception.BadRequestException;
import truphone.device.exception.DeviceNotFoundException;
import truphone.device.model.DeviceDTO;
import truphone.device.service.DeviceService;

import java.util.List;

@RestController
@RequestMapping("devices")
@Slf4j
public class DeviceController {

    private final DeviceService deviceService;

    @Autowired
    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @GetMapping("/{deviceId}")
    public @ResponseBody
    DeviceDTO getDevice(@PathVariable String deviceId) throws DeviceNotFoundException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            log.info("Fetching device with id {}", deviceId);
            return deviceService.getDevice(deviceId);
        } finally {
            stopWatch.stop();
            log.info("Took {}ms to fetch item with id {}", stopWatch.getTotalTimeMillis(), deviceId);
        }
    }

    @PostMapping
    public @ResponseBody
    DeviceDTO saveDevice(@RequestBody DeviceDTO device) throws BadRequestException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            log.info("Received device with name {} and brand {} to save", device.getDeviceName(), device.getDeviceBrand());
            return deviceService.saveDevice(device);
        } finally {
            stopWatch.stop();
            log.info("Took {}ms to save item", stopWatch.getTotalTimeMillis());
        }
    }

    @PutMapping("/{deviceId}")
    public @ResponseBody
    DeviceDTO updateDevice(@PathVariable String deviceId, @RequestBody DeviceDTO device) throws BadRequestException, DeviceNotFoundException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            log.info("Received device {} to update", device);
            return deviceService.updateDevice(deviceId, device);
        } finally {
            stopWatch.stop();
            log.info("Took {}ms to update item {} with id {}", stopWatch.getTotalTimeMillis(), device, deviceId);
        }
    }

    // both patch and put do the same thing, just here as a formality to respect REST trends
    @PatchMapping("/{deviceId}")
    public @ResponseBody
    DeviceDTO patchDevice(@PathVariable String deviceId, @RequestBody DeviceDTO device) throws BadRequestException, DeviceNotFoundException {
        return updateDevice(deviceId, device);
    }

    @DeleteMapping("{deviceId}")
    public void deleteDevice(@PathVariable String deviceId) throws DeviceNotFoundException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            log.info("Fetching device with id {}", deviceId);
            deviceService.deleteDevice(deviceId);
        } finally {
            stopWatch.stop();
            log.info("Took {}ms to delete item with id {}", stopWatch.getTotalTimeMillis(), deviceId);
        }
    }

    @GetMapping
    public List<DeviceDTO> getAll() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            return deviceService.getAll();
        } finally {
            stopWatch.stop();
            log.info("Took {}ms to fetch all entries", stopWatch.getTotalTimeMillis());
        }
    }
}

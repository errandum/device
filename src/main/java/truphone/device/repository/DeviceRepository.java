package truphone.device.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import truphone.device.model.Device;

import java.util.List;
import java.util.UUID;

@Repository
public interface DeviceRepository extends CrudRepository<Device, UUID> {
    List<Device> findByDeviceBrandStartsWith(String deviceBrand);
}

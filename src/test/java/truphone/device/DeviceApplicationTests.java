package truphone.device;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import truphone.device.model.Device;
import truphone.device.model.DeviceDTO;
import truphone.device.repository.DeviceRepository;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class DeviceApplicationTests {

	@Autowired
	MockMvc mvc;

	@Autowired
	ObjectMapper mapper;

	@Autowired
	DeviceRepository deviceRepository;

	@Test
	void contextLoads() throws Exception {

		// This is a very very basic test that saves and gets information that has been saved in multiple ways
		// It is getting late, but I thought it was better to have them than not
		DeviceDTO deviceDTO = new DeviceDTO();
		deviceDTO.setDeviceBrand("brand");
		deviceDTO.setDeviceName("name");
		DeviceDTO deviceDTOTwo = new DeviceDTO();
		deviceDTOTwo.setDeviceBrand("brand2");
		deviceDTOTwo.setDeviceName("name2");

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(deviceDTO));

		MockHttpServletRequestBuilder mockRequestTwo = MockMvcRequestBuilders.post("/devices")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(deviceDTOTwo));

		mvc.perform(mockRequest).andExpect(status().isOk());
		mvc.perform(mockRequestTwo).andExpect(status().isOk());

		List<Device> devices = new ArrayList<>();
		deviceRepository.findAll().forEach(devices::add);
		assertThat(devices).hasSize(2);

		Device toTest = devices.get(0);

		MockHttpServletRequestBuilder mockRequestGet =
				MockMvcRequestBuilders.get("/devices/{deviceId}",toTest.getDeviceId().toString())
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON);

		mvc.perform(mockRequestGet)
				.andExpect(status().isOk())
				.andExpect(content().string(containsString(toTest.getDeviceName())))
				.andExpect(content().string(containsString(toTest.getDeviceBrand())));
	}
}

package truphone.device.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import truphone.device.model.DeviceDTO;
import truphone.device.service.DeviceService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DeviceControllerTest {

    private final DeviceDTO deviceDto = new DeviceDTO(
            UUID.randomUUID(),
            "deviceName",
            "deviceBrand",
            LocalDateTime.now()
    );

    private final DeviceDTO deviceDtoExtra = new DeviceDTO(
            UUID.randomUUID(),
            "deviceName2",
            "deviceBrand2",
            LocalDateTime.now()
    );

    List<DeviceDTO> deviceList = List.of(deviceDto, deviceDtoExtra);

    @Test
   void testGetDevice() throws Exception {
        DeviceService deviceService = mock(DeviceService.class);
        when(deviceService.getDevice(anyString())).thenReturn(deviceDto);

        DeviceController controller = new DeviceController(deviceService);
        DeviceDTO result = controller.getDevice(deviceDto.getDeviceId().toString());

        verify(deviceService, times(1)).getDevice(deviceDto.getDeviceId().toString());
        assertThat(result).isEqualTo(deviceDto);
    }

    @Test
    void testSaveDevice() throws Exception {
        DeviceService deviceService = mock(DeviceService.class);
        when(deviceService.saveDevice(any(DeviceDTO.class))).thenReturn(deviceDto);

        DeviceController controller = new DeviceController(deviceService);
        DeviceDTO result = controller.saveDevice(deviceDto);

        verify(deviceService, times(1)).saveDevice(deviceDto);
        assertThat(result).isEqualTo(deviceDto);
    }

    @Test
    void testUpdateDevice() throws Exception {
        DeviceService deviceService = mock(DeviceService.class);
        when(deviceService.updateDevice(anyString(), any(DeviceDTO.class))).thenReturn(deviceDto);

        DeviceController controller = new DeviceController(deviceService);
        DeviceDTO result = controller.updateDevice(deviceDto.getDeviceId().toString(),deviceDto);

        verify(deviceService, times(1)).updateDevice(deviceDto.getDeviceId().toString(),deviceDto);
        assertThat(result).isEqualTo(deviceDto);
    }

    @Test
    void testPatchDevice() throws Exception {
        DeviceService deviceService = mock(DeviceService.class);
        when(deviceService.updateDevice(anyString(), any(DeviceDTO.class))).thenReturn(deviceDto);

        DeviceController controller = new DeviceController(deviceService);
        DeviceDTO result = controller.patchDevice(deviceDto.getDeviceId().toString(),deviceDto);

        verify(deviceService, times(1)).updateDevice(deviceDto.getDeviceId().toString(),deviceDto);
        assertThat(result).isEqualTo(deviceDto);
    }

    @Test
    void testDeleteDevice() throws Exception {
        DeviceService deviceService = mock(DeviceService.class);
        doNothing().when(deviceService).deleteDevice(anyString());

        DeviceController controller = new DeviceController(deviceService);
        controller.deleteDevice(deviceDto.getDeviceId().toString());

        verify(deviceService, times(1)).deleteDevice(deviceDto.getDeviceId().toString());
    }

    @Test
    void testGetAllDevices() throws Exception {
        DeviceService deviceService = mock(DeviceService.class);
        when(deviceService.getAll()).thenReturn(deviceList);

        DeviceController controller = new DeviceController(deviceService);
        List<DeviceDTO> result = controller.getAll();

        verify(deviceService, times(1)).getAll();
        assertThat(result).containsAll(deviceList);
    }
}

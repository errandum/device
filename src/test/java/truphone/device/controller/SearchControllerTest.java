package truphone.device.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import truphone.device.model.DeviceDTO;
import truphone.device.service.DeviceService;
import truphone.device.service.SearchService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SearchControllerTest {

    private final DeviceDTO deviceDto = new DeviceDTO(
            UUID.randomUUID(),
            "deviceName",
            "deviceBrand",
            LocalDateTime.now()
    );

    private final DeviceDTO deviceDtoExtra = new DeviceDTO(
            UUID.randomUUID(),
            "deviceName2",
            "deviceBrand2",
            LocalDateTime.now()
    );

    List<DeviceDTO> deviceList = List.of(deviceDto, deviceDtoExtra);

    @Test
   void testSearchByBrand() throws Exception {
        SearchService searchService = mock(SearchService.class);
        when(searchService.getDeviceByBrand(anyString())).thenReturn(deviceList);

        SearchController controller = new SearchController(searchService);
        List<DeviceDTO> result = controller.getDeviceByBrand("brand");

        verify(searchService, times(1)).getDeviceByBrand("brand");
        assertThat(result).containsAll(deviceList);
    }
}

package truphone.device.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import truphone.device.model.Device;
import truphone.device.model.DeviceDTO;
import truphone.device.repository.DeviceRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SearchServiceTest {

    private final DeviceDTO deviceDto = new DeviceDTO(
            UUID.randomUUID(),
            "deviceName",
            "deviceBrand",
            LocalDateTime.now()
    );

    private final Device device = new Device(
            deviceDto.getDeviceId(),
            deviceDto.getDeviceName(),
            deviceDto.getDeviceBrand(),
            deviceDto.getDeviceCreationTime()
    );

    private final DeviceDTO deviceDtoExtra = new DeviceDTO(
            UUID.randomUUID(),
            "deviceName2",
            "deviceBrand2",
            LocalDateTime.now()
    );

    private final Device deviceExtra = new Device(
            deviceDtoExtra.getDeviceId(),
            deviceDtoExtra.getDeviceName(),
            deviceDtoExtra.getDeviceBrand(),
            deviceDtoExtra.getDeviceCreationTime()
    );

    List<DeviceDTO> deviceDtoList = List.of(deviceDto, deviceDtoExtra);
    List<Device> deviceList = List.of(device, deviceExtra);

    @Test
    void testGetDeviceByBrand() throws Exception {
        DeviceRepository deviceRepository = mock(DeviceRepository.class);
        when(deviceRepository.findByDeviceBrandStartsWith("brand")).thenReturn(deviceList);

        SearchService service = new SearchService(deviceRepository);
        List<DeviceDTO> result = service.getDeviceByBrand("brand");

        verify(deviceRepository, times(1)).findByDeviceBrandStartsWith("brand");
        assertThat(result).containsAll(deviceDtoList);
    }
}

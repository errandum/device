package truphone.device.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import truphone.device.controller.DeviceController;
import truphone.device.model.Device;
import truphone.device.model.DeviceDTO;
import truphone.device.repository.DeviceRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DeviceServiceTest {

    private final DeviceDTO deviceDto = new DeviceDTO(
            UUID.randomUUID(),
            "deviceName",
            "deviceBrand",
            LocalDateTime.now()
    );

    private final Device device = new Device(
            deviceDto.getDeviceId(),
            deviceDto.getDeviceName(),
            deviceDto.getDeviceBrand(),
            deviceDto.getDeviceCreationTime()
    );

    private final DeviceDTO deviceDtoExtra = new DeviceDTO(
            UUID.randomUUID(),
            "deviceName2",
            "deviceBrand2",
            LocalDateTime.now()
    );

    private final Device deviceExtra = new Device(
            deviceDtoExtra.getDeviceId(),
            deviceDtoExtra.getDeviceName(),
            deviceDtoExtra.getDeviceBrand(),
            deviceDtoExtra.getDeviceCreationTime()
    );

    List<DeviceDTO> deviceDtoList = List.of(deviceDto, deviceDtoExtra);
    List<Device> deviceList = List.of(device, deviceExtra);

    @Test
   void testGetDevice() throws Exception {
        DeviceRepository deviceRepository = mock(DeviceRepository.class);
        UUID id = device.getDeviceId();
        when(deviceRepository.findById(id)).thenReturn(Optional.of(device));

        DeviceService service = new DeviceService(deviceRepository);
        DeviceDTO result = service.getDevice(device.getDeviceId().toString());

        verify(deviceRepository, times(1)).findById(id);
        assertThat(result).isEqualTo(deviceDto);
    }

    @Test
    void testSaveDevice() throws Exception {
        DeviceRepository deviceRepository = mock(DeviceRepository.class);
        // the date is dynamically created, so can't test for specific saved device
        when(deviceRepository.save(any(Device.class))).thenReturn(device);

        DeviceService service = new DeviceService(deviceRepository);
        DeviceDTO result = service.saveDevice(deviceDto);

        // same as above. The id and date are generated on save
        verify(deviceRepository, times(1)).save(any(Device.class));

        assertThat(result.getDeviceName()).isEqualTo(deviceDto.getDeviceName());
        assertThat(result.getDeviceBrand()).isEqualTo(deviceDto.getDeviceBrand());
    }

    @Test
    void testUpdateDevice() throws Exception {
        DeviceRepository deviceRepository = mock(DeviceRepository.class);
        when(deviceRepository.findById(device.getDeviceId())).thenReturn(Optional.of(device));
        when(deviceRepository.save(device)).thenReturn(device);

        DeviceService service = new DeviceService(deviceRepository);
        DeviceDTO result = service.updateDevice(deviceDto.getDeviceId().toString(),deviceDto);

        verify(deviceRepository, times(1)).save(device);
        verify(deviceRepository, times(1)).findById(deviceDto.getDeviceId());
        assertThat(result).isEqualTo(deviceDto);
    }

    @Test
    void testDeleteDevice() throws Exception {
        DeviceRepository deviceRepository = mock(DeviceRepository.class);
        doNothing().when(deviceRepository).deleteById(device.getDeviceId());

        DeviceService service = new DeviceService(deviceRepository);
        service.deleteDevice(deviceDto.getDeviceId().toString());

        verify(deviceRepository, times(1)).deleteById(device.getDeviceId());
    }

    @Test
    void testGetAllDevices() throws Exception {
        DeviceRepository deviceRepository = mock(DeviceRepository.class);
        when(deviceRepository.findAll()).thenReturn(deviceList);

        DeviceService service = new DeviceService(deviceRepository);
        List<DeviceDTO> result = service.getAll();

        verify(deviceRepository, times(1)).findAll();
        assertThat(result).containsAll(deviceDtoList);
    }
}
